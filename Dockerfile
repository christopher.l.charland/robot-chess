FROM ubuntu:20.04

RUN apt-get update
RUN apt-get install -y python3-pip

WORKDIR /robot-chess

COPY . /robot-chess
RUN pip3 install -r requirements.txt

CMD uvicorn app.main:app --host 0.0.0.0
