#!/usr/bin/env python3
import argparse
import requests
from app.models import RegisterUser, Token

try:
    from app import db
except:
    print('Service currently running, unable to set or remove admins.')

SERVER = 'http://127.0.0.1:8000'
# credentials
USERNAME = 'ProfChaos'
PASSWORD = 'mysterypass'


def login(user, password):
    user = {
        'username': user,
        'password': password
    }
    return requests.post(f'{SERVER}/token', data=user).json()


def set_admin(username: str):
    user = db.get_user(username)
    user.is_admin = True
    db.update_user(user)


def remove_admin(username: str):
    user = db.get_user(username)
    user.is_admin = False
    db.update_user(user)


def delete_user(token: Token, username: str):
    data = {}
    data['token'] = token
    return requests.delete(f'{SERVER}/user/{username}/delete', json=token)


def register(full_name, password, username=None, email=None):
    new_user = RegisterUser(username=username,
                            password=password,
                            full_name=full_name,
                            email=email)

    db.create_user(new_user)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Administration tool for robot chess.')
    parser.add_argument('--set-admin',
                        help='Sets a user to be an administrator',
                        dest='set_admin',
                        default=None)
    parser.add_argument('--remove-admin',
                        help='Removes administrator privilege from a user.',
                        dest='remove_admin',
                        default=None)

    parser.add_argument('--make-default-user',
                        help='Sets up a default user',
                        dest='make_default_user',
                        default=False,
                        action='store_true')

    parser.add_argument('--delete-user',
                        help='Deletes a user',
                        dest='delete_user',
                        default=None)

    args = parser.parse_args()

    if args.set_admin:
        set_admin(args.set_admin)

    if args.remove_admin:
        remove_admin(args.remove_admin)

    if args.make_default_user:
        register(
            'Butters',
            'mysterypass',
            'ProfChaos',
            'l.stotch@southpark.co.uk'
        )
        set_admin('ProfChaos')

    if args.delete_user:
        token = login(USERNAME, PASSWORD)

        delete_user(token, args.delete_user)
