import requests

SERVER = 'http://127.0.0.1:8000'


def register(full_name, password, username=None, email=None):
    user = {
        'username': username,
        'full_name': full_name,
        'email': email,
        'password': password,
    }
    requests.post(f'{SERVER}/register', json=user)


def new_game(token):
    game = requests.get(f'{SERVER}/game', json=token)
    game_id = game.json()['game_id']
    fen_str = game.json()['fen_str']
    return game_id, fen_str


def login(user, password):
    user = {
        'username': user,
        'password': password
    }
    return requests.post(f'{SERVER}/token', data=user).json()


def move(token, game_id, uci_str):
    data = {}
    data['token'] = token
    data['move'] = {'game_id': game_id, 'uci_str': uci_str}
    return requests.post(f'{SERVER}/game/{game_id}', json=data)


def get_ascii(game_id):
    return requests.get(f'{SERVER}/game/{game_id}/ascii').text


def get_svg(game_id):
    return requests.get(f'{SERVER}/game/{game_id}/svg').text


def get_game_status(game_id):
    return requests.get(f'{SERVER}/game/{game_id}')


def main():
    pass


if __name__ == '__main__':
    # Registers a new user
    register(
        'Dave Ryder',
        'sciencepass',
        'Bolt VanderHuge',
        'splint@chesthair.tv'
    )

    register(
        'Dave Ryder',
        'mysterypass',
        'Thick McRunfast',
        'blast@hardcheese.io'
    )

    # Log in and get a token
    token = login('Bolt VanderHuge', 'sciencepass')
    token2 = login('Thick McRunfast', 'mysterypass')

    # Use token to identify yourself and start a new game
    game_id, start_positions = new_game(token)

    game_id2, start_positions2 = new_game(token2)

    print(game_id, game_id2)

    print('Playing through the Scholar\'s mate')

    # Make first move
    move(token, game_id, 'e2e4')
    print(get_ascii(game_id))

    move(token2, game_id2, 'e7e5')
    print(get_ascii(game_id))

    move(token, game_id, 'd1h5')
    print(get_ascii(game_id))

    move(token2, game_id2, 'b8c6')
    print(get_ascii(game_id))

    move(token, game_id, 'f1c4')
    print(get_ascii(game_id))

    move(token2, game_id2, 'g8f6')
    print(get_ascii(game_id))

    move(token, game_id, 'h5f7')
    print(get_ascii(game_id))

    move(token2, game_id2, 'a7a6')
    print(get_ascii(game_id))

    print(get_game_status(game_id).text)
    print(get_ascii(game_id))
    print(get_svg(game_id))
