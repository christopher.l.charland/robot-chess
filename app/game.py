import chess
import chess.svg


class IllegalMove(BaseException):
    pass


class GameOver(BaseException):
    pass


class UnkChessError(BaseException):
    pass


class ChessGame(chess.Board):
    def __init__(self):
        super().__init__()

    @property
    def fen_str(self):
        return self.fen()

    @property
    def svg(self):
        return chess.svg.board(self)

    @property
    def turn_str(self):
        if self.turn == chess.WHITE:
            return 'w'
        elif self.turn == chess.BLACK:
            return 'b'
        else:
            return '?'

    @property
    def ascii(self):
        return str(self)

    def make_move(self, uci_str):
        try:
            self.push_uci(uci_str)
        except ValueError:
            if self.is_game_over():
                raise GameOver
            raise IllegalMove

