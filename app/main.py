"""
Comments on functions in this module are not suitable for reference
unless you are already familiar with FastAPI conventions.  Function
parameters are not necessarily things that a user of the API sends
in their message, some are passed in by the framework to supply
and/or modify context of the request.  For an API reference run the
server and head to /docs
"""

from fastapi import FastAPI, Response
from fastapi.staticfiles import StaticFiles

from app.routers import game, user, manage

boards = {}

app = FastAPI(
    title='Bottle Royale with Chess',
    description='Robots locked in eternal combot.',
    version='0.1.0',
    redoc_url=None
)
app.mount('/static', StaticFiles(directory='app/static'), name='static')
app.include_router(game.router)
app.include_router(user.router)
app.include_router(manage.router)


# Top level routes
@app.get('/', response_model=str, tags=['Static Pages'])
async def root():
    """
    This returns whatever is on the home page ( GET / ).
    It should contain instructions on how to find the documentation
    and a short description of the project.
    """
    response_str = f'This is an API for robots to play chess. ' \
                   f'Please be respectful and don\'t try to pwn it.<br><br>' \
                   f'documentation is at /docs'
    return Response(content=response_str, media_type='text/html')


def main():
    print('Dont run me like this.')


if __name__ == '__main__':
    main()
