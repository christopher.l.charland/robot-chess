from fastapi import APIRouter, Depends, status, HTTPException, Response
from fastapi.security import OAuth2PasswordRequestForm

from app.crypto import ACCESS_TOKEN_EXPIRE_MINUTES, create_access_token
from app.dependencies import authenticate_user, get_current_active_user
from app.models import Token, User, RegisterUser
from app import db

from datetime import timedelta

router = APIRouter(
    tags=['admin']
)


@router.post("/token", response_model=Token)
async def login_for_access_token(form_data: OAuth2PasswordRequestForm = Depends()):
    user = authenticate_user(form_data.username, form_data.password)
    if not user:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Bearer"},
        )
    access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
    access_token = create_access_token(
        data={"sub": user.username}, expires_delta=access_token_expires
    )
    return {"access_token": access_token, "token_type": "bearer"}


@router.post('/register')
def register_user(user_info: RegisterUser, response: Response):
    """
    This endpoint is for creating a new user/bot.
    """
    illegal_users = {'DRAW', 'ERROR'}
    if user_info.username in illegal_users:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'Illegal user name.')
    try:
        db.create_user(user_info)
    except db.UserExists:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'User already exists.')
    response.status_code = status.HTTP_201_CREATED


@router.get('/users')
def list_users():
    return db.user_db


@router.get('/games')
def list_users():
    return db.game_archive


@router.get("/users/me/", response_model=User)
async def read_users_me(current_user: User = Depends(get_current_active_user)):
    return current_user
