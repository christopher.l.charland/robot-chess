from fastapi import APIRouter, HTTPException, status, Form, Response
from fastapi.responses import HTMLResponse
from app.models import User, Token
from app.dependencies import get_current_user
from app import db

router = APIRouter(
    prefix='/user',
    tags=['user management']
)


@router.get('/{user_name}', response_model=User)
def user_info(user_name: str):
    """
    Gets json object describing a user
    """
    try:
        user = db.get_user(user_name)
    except db.UserNotExists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'User not found.')
    return user


@router.get('/{user_name}/stats', response_class=HTMLResponse)
def user_stats(user_name: str):
    try:
        user = db.get_user(user_name)
    except db.UserNotExists:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail=f'User not found.')

    response_html = f'<html><head><title>Stats for {user.username}' \
                    f'</title><link href="/static/normalize.css" rel="stylesheet">' \
                    f'<link href="/static/skeleton.css" rel="stylesheet">' \
                    f'<link rel="preconnect" href="https://fonts.gstatic.com">' \
                    f'<link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">' \
                    f'</head><body class="container">' \
                    f'<h1><center>{user.username}</center></h1>'

    wins = 0
    losses = 0
    draws = 0
    table = '''<table class="u-full-width">
  <thead>
    <tr>
      <th>Game ID</th>
      <th>White</th>
      <th>Black</th>
      <th>Result</th>
    </tr>
  </thead><tbody>'''
    for game_id in user.results:
        game = db.get_game(game_id)
        if game.winner == user_name:
            wins += 1
        elif game.winner == 'DRAW':
            draws += 1
        else:
            losses += 1
        table += f'<tr><td>{str(game_id)}</td>' \
                 f'<td>{game.white}</td><td>{game.black}</td><td>{game.winner}</td></tr>'

    response_html += f'<div class="row">' \
                     f'<div class="one column">' \
                     f'Wins<br>' \
                     f'Losses<br>' \
                     f'Draws<br>' \
                     f'</div>'
    response_html += f'<div class="one column">' \
                     f'{wins}<br>' \
                     f'{losses}<br>' \
                     f'{draws}<br>' \
                     f'</div></div>'

    response_html += table
    response_html += f'</body></html>'

    return response_html


@router.delete('/{user_name}/delete')
async def delete_user(user_name: str, token: Token):
    print(token)
    requesting_user = await get_current_user(token.access_token)
    if not requesting_user.is_admin:
        raise HTTPException(status_code=status.HTTP_403_FORBIDDEN,
                            detail=f'I can\'t let you do that dave...')
    elif requesting_user.is_admin:
        db.delete_user(user_name)
