from fastapi import APIRouter, HTTPException, status, Form, Response

from uuid import uuid4, UUID
import app.game as chess

from app.dependencies import get_current_user
from ..models import Token, Game, Move
from app import db

router = APIRouter(
    prefix='/game',
    tags=['gameplay'],
)


class Matcher:
    def __init__(self):
        self.unmatched_game = None

    def match_game(self, username: str):
        if self.unmatched_game:
            matched = db.get_game(self.unmatched_game)
            # set username for black
            matched.black = username
            db.update_game(matched.game_id, matched)

            # associate the game with the user
            user = db.get_user(username)
            user.results.append(matched.game_id)
            db.update_user(user)

            # reset unmatched_game
            self.unmatched_game = None
            return matched
        else:
            new_game = db.create_game()
            new_game.white = username
            db.update_game(new_game.game_id, new_game)
            user = db.get_user(username)
            user.results.append(new_game.game_id)
            db.update_user(user)
            self.unmatched_game = new_game.game_id
            return new_game


match_maker = Matcher()


@router.get('', response_model=Game)
async def new_game(token: Token, response: Response):
    """
    Generates a new game and returns a JSON object describing it.  Initially the result will be "*"
    indicating that the result is undetermined.  A user or bot will have to check the value of the
    "black" and "white" fields to see which player they are.  If the new game object indicates that
    you are the black player, the game begins immediately, if its indicates you are the white player,
    you can send an initial move but the game will not start until a player is assigned to black.
    """
    requesting_user = await get_current_user(token.access_token)
    game = match_maker.match_game(requesting_user.username)

    if not game.black:
        response.status_code = status.HTTP_201_CREATED
    else:
        response.status_code = status.HTTP_200_OK

    return Game(**dict(game))


@router.post('/{game_id}')
async def move(game_id: UUID,
               token: Token,
               move: Move):
    """
    Posting to a game/game_id makes a move
    Args:
        game_id: UUID - the UUID of the game, this is part of the URL
        user: User - the User information
        uci_str: str - the move being made, in UCI format submitted as form data

    UCI format for moves https://en.wikipedia.org/wiki/Universal_Chess_Interface
    """
    requesting_user = await get_current_user(token.access_token)

    try:
        game = db.get_game(game_id)
        board = db.get_board(game.board_id)
    except LookupError:
        raise HTTPException(status_code=status.HTTP_507_INSUFFICIENT_STORAGE, detail='Missing game data.')

    if board.turn_str == 'w':
        if game.white != requesting_user.username:
            raise HTTPException(status_code=status.HTTP_425_TOO_EARLY,
                                detail=f'Not your move {requesting_user.username}')
    elif board.turn_str == 'b':
        if game.black != requesting_user.username:
            raise HTTPException(status_code=status.HTTP_425_TOO_EARLY,
                                detail=f'Not your move {requesting_user.username}')

    try:
        board.make_move(move.uci_str)
    except chess.IllegalMove:
        raise HTTPException(status_code=status.HTTP_409_CONFLICT, detail='Illegal move.')
    except chess.GameOver:
        db.archive_game(game_id)
        raise HTTPException(status_code=status.HTTP_410_GONE, detail='Game Over')

    game.moves.append(move.uci_str)
    game.fen_str = board.fen_str
    db.update_game(game_id, game)
    db.update_board(game.board_id, board)


@router.get('/{game_id}', response_model=Game)
def current_game_state(game_id: UUID):
    """
    Get to the game/game_id retrieves the current state of the board in FEN notation
    Args:
        game_id: UUID - the UUID of the game, this is part of the URL

    FEN notation reference https://en.wikipedia.org/wiki/Forsyth%E2%80%93Edwards_Notation
    """

    try:
        game = db.get_game(game_id)
    except LookupError:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Game not found.')

    # TODO: result is never update when the game is over

    return game


@router.get('/{game_id}/svg', response_model=str)
def game_svg(game_id: UUID):
    """
    Get to game/game_id/svg returns an SVG of the board, suitable for display to a human.
    Args:
        game_id: UUID - the UUID of the game, this is part of the URL
    """
    game = db.get_game(game_id)
    if game.result == '*':  # Doesnt get update until game is over
        board = db.get_board(game.board_id)
        return Response(content=board.svg, media_type='application/xml')
    else:
        return Response(content=game.svg, media_type='application/xml')


@router.get('/{game_id}/ascii', response_model=str)
def game_ascii(game_id: UUID):
    """
    Get to game/game_id/ascii returns an ascii representation of the board, this is provided
    as a convenience endpoint for people who may want to create a terminal interface.
    Args:
        game_id: UUID - the UUID of the game, this is part of the URL
    """
    game = db.get_game(game_id)
    if game.result == '*':  # Doesnt get update until game is over
        board = db.get_board(game.board_id)
        return Response(board.ascii, media_type='text/html')
    else:
        return Response(game.ascii, media_type='text/html')
