from pydantic import BaseModel, Field, EmailStr
from typing import Optional, List
from uuid import UUID
from fastapi import Body


class Game(BaseModel):
    game_id: UUID
    fen_str: str
    white: str
    black: str
    moves: List[str]
    result: str

    class Config:
        schema_extra = {
            'example': {
                "game_id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "fen_str": "r1bqkb1r/pppp1Qpp/2n2n2/4p3/2B1P3/8/PPPP1PPP/RNB1K1NR b KQkq - 0 4",
                "white": "Bolt VanderHuge",
                "black": "Thick McRunfast",
                "moves": [
                    "e2e4", "e7e5", "d1h5", "b8c6", "f1c4", "g8f6", "h5f7"
                ],
                "result": "1-0"
            }
        }


class GameInDB(Game):
    board_id: UUID


class GameArchive(Game):
    winner: str
    ascii: str
    svg: str


class Token(BaseModel):
    access_token: str
    token_type: str

    class Config:
        schema_extra = {
            'example': {
                'access_token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiJjaGFkc3RlciIsImV4cCI6MTYwOTg2MDg5Nn0.xjFZ5E00fx9btkAYDtmBXdz1_U19L3JV378-5_IOeP0',
                'token_type': 'bearer',

            }
        }


class Move(BaseModel):
    game_id: UUID
    uci_str: str

    class Config:
        schema_extra = {
            'example': {
                "game_id": "3fa85f64-5717-4562-b3fc-2c963f66afa6",
                "uci_str": "e2e4"
            }
        }


class TokenData(BaseModel):
    username: Optional[str] = None


class RegisterUser(BaseModel):
    username: str = Body(
        ..., title='The username that will be displayed to other users.', max_length=60
    )
    password: str = Body(
        ..., title='Desired password, It\'s never stored in plaintext but I wrote this API for shits and giggles...'
                   'do what you want.',
        min_length=8
    )
    full_name: str = Body(
        None, title='Full (real) name, only needed if you want to claim the account as yours.',
        max_length=60
    )

    email: Optional[EmailStr] = Body(
        None, title='If you want to be contacted'
    )

    class Config:
        schema_extra = {
            'example': {
                'username': 'Bolt VanderHuge',
                'password': 'sciencepass',
                'full_name': 'Dave Ryder',
                'email': 'splint@chesthair.tv'
            }
        }


class User(BaseModel):
    username: str = Body(
        ..., title='The username that will be displayed to other users.',
        max_length=60
    )
    email: Optional[str] = Body(
        None,
        title='The email account to associate with the username.'
    )
    full_name: Optional[str] = Body(
        None,
        title='Full (real) name, only needed if you want to claim the account as yours.',
        max_length=60
    )
    disabled: Optional[bool] = Body(
        False,
        title='True if the account has been disabled. I will do this if a bot has gone rogue'
              'and is making too many requests (hundreds or more per second probably).'
    )
    results: List[UUID] = Body(
        ...,
        title='A list of game UUID\'s the account has participated in.'
    )

    class Config:
        schema_extra = {
            'example': {
                'username': 'Bolt VanderHuge',
                'email': 'splint@chesthair.tv',
                'full_name': 'Dave Ryder',
                'disabled': 'False',
                'result': ['823fdbb3-bf7f-499a-8160-d4483ce03163']
            }
        }


class UserInDB(User):
    hashed_password: str
    is_admin: bool
