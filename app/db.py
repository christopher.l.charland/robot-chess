import shelve
from pathlib import Path
from uuid import UUID, uuid4

from app.models import UserInDB, RegisterUser, GameInDB, GameArchive, User
from app import crypto
from app import game

DATA_DIR = 'app/data'

Path(DATA_DIR).mkdir(parents=True, exist_ok=True)

game_db = shelve.open(f'{DATA_DIR}/games.db', flag='c', writeback=True)
user_db = shelve.open(f'{DATA_DIR}/users.db', flag='c', writeback=True)
board_db = shelve.open(f'{DATA_DIR}/boards.db', flag='c', writeback=True)
game_archive = shelve.open(f'{DATA_DIR}/game_archive.db', flag='c', writeback=True)


# oh shit its CRUD
def create_board():
    board_id = uuid4()
    board_db[str(board_id)] = game.ChessGame()
    return board_id


def get_board(board_id: UUID):
    if str(board_id) in board_db:
        return board_db[str(board_id)]
    else:
        raise LookupError


def update_board(board_id, board):
    if str(board_id) in board_db:
        board_db[str(board_id)] = board
        board_db.sync()


def delete_board(board_id):
    if str(board_id) in board_db:
        del board_db[str(board_id)]
        board_db.sync()


def create_game():
    board_id = create_board()
    board = get_board(board_id)

    new_game = GameInDB(game_id=uuid4(),
                        board_id=board_id,
                        fen_str=board.fen_str,
                        white='',
                        black='',
                        moves=[],
                        result=board.result()
                        )

    game_db[str(new_game.game_id)] = new_game
    print(new_game)
    return new_game


def get_game(game_id: UUID):
    if str(game_id) in game_db:
        game_obj = game_db[str(game_id)]
        return game_obj

    elif str(game_id) in game_archive:
        game_obj = game_archive[str(game_id)]
        return game_obj
    else:
        raise LookupError


def update_game(game_id: UUID, game_data: GameInDB):
    if str(game_id) in game_db:
        game_db[str(game_id)] = game_data
        game_db.sync()


def archive_game(game_id):
    game = get_game(game_id)
    board = get_board(game.board_id)
    result = board.result()
    if result != '*':
        if result == '1-0':
            winner = game.white
        elif result == '0-1':
            winner = game.black
        elif result == '1/2-1/2':
            winner = 'DRAW'
        else:
            winner = 'ERROR'
        game_archive[str(game_id)] = GameArchive(game_id=game.game_id,
                                                 fen_str=game.fen_str,
                                                 white=game.white,
                                                 black=game.black,
                                                 moves=game.moves,
                                                 result=board.result(),
                                                 ascii=board.ascii,
                                                 svg=board.svg,
                                                 winner=winner)
        del game_db[str(game_id)]
        game_archive.sync()
        game_db.sync()


def create_user(user_info: RegisterUser):
    if user_info.username in user_db:
        raise UserExists
    hashed_pw = crypto.get_password_hash(user_info.password)
    new_user = UserInDB(username=user_info.username,
                        email=user_info.email,
                        full_name=user_info.full_name,
                        hashed_password=hashed_pw,
                        disabled=False,
                        results=[],
                        is_admin=False)
    user_db[user_info.username] = new_user
    user_db.sync()


def get_user(username: str):
    if username in user_db:
        user = user_db[username]
        return user
    else:
        raise UserNotExists


def update_user(user: User):
    if user.username in user_db:
        user_db[user.username] = user
        user_db.sync()
    else:
        raise UserNotExists


def delete_user(username: str):
    if username in user_db:
        del user_db[username]
    else:
        raise UserNotExists


class UserNotExists(BaseException):
    pass


class UserExists(BaseException):
    pass
