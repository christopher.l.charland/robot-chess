# Chess for robots

### Usage

`pip3 install -r requirements.txt`

Then to start the server on 127.0.0.1:8000

`uvicorn app.main:app`

or if you're doing development

`uvicorn app.main:app --reload`

That will automatically reload when changes to the project are detected.
Documentation will be located at http://127.0.0.1:8000/docs

The example client will register 2 users, both will log in and request a game, they'll be matched with each other and play through the scholars mate.

#### Docker

Create and run docker container from the provided Dockerfile:

```
$ docker build -t robot-chess:latest .
$ docker run -it --rm -p 8000:8000 robot-chess:latest
```

From host, browse to the IP address of the docker service (interface is likely
named `docker0` in an `ipconfig` listing) on port 8000.
